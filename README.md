# Laravel Migration Tasks

[![Latest Version on Packagist](https://img.shields.io/packagist/v/thomascombe/laravel-migration-tasks.svg?style=flat-square)](https://packagist.org/packages/thomascombe/laravel-migration-tasks)
[![Total Downloads](https://img.shields.io/packagist/dt/thomascombe/laravel-migration-tasks.svg?style=flat-square)](https://packagist.org/packages/thomascombe/laravel-migration-tasks)

This package allow you to run some tasks during deployment, it's just like Laravel Migration but with tasks (create entities, run artisan task, Algolia reindex...) instead of update database schema.

## Installation

You can install the package via composer:

```bash
composer require thomascombe/laravel-migration-tasks
```

## Usage

Create new migration task
``` bash
php artisan make:migration-task
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Credits

- [Thomas Combe](https://github.com/thomascombe)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
