<?php

namespace Thomascombe\LaravelMigrationTasks\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class MakeMigrationTask extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:migration-task {name : The name of the migration}
        {--flag= : The migration flag (ex: before, after)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new migration task';
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->getNameInput();
        $class = $this->buildClass($name);

        $this->makeDirectory($name);
        return $this->filesystem->put($this->getPath($name), $class);
    }

    /**
     * Get the desired class name from the input.
     *
     * @return string
     */
    protected function getNameInput()
    {
        return Str::studly(trim($this->argument('name')));
    }

    /**
     * Build the class with the given name.
     *
     * @param string $name
     *
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->filesystem->get($this->getStub());

        $this->replaceNamespace($stub)->replaceClassname($stub);

        return $stub;
    }

    /**
     * Replace dummy namespace
     *
     * @param $stub
     * @return $this
     */
    protected function replaceNamespace(&$stub)
    {
        $stub = str_replace(['DummyNamespace'], [$this->getNamespace()], $stub);

        return $this;
    }

    /**
     * Replace dummy class name
     *
     * @param $stub
     * @return $this
     */
    protected function replaceClassname(&$stub)
    {
        $stub = str_replace(['DummyMigrationTasksClass'], [$this->getNameInput()], $stub);

        return $this;
    }

    /**
     * Get migration task namespace
     *
     * @return string
     */
    protected function getNamespace()
    {
        return sprintf('%s%s', app()->getNamespace(), 'Deploy\\Tasks');
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = sprintf(
            '%s\\%s_%s',
            str_replace('App', 'app', $this->getNamespace()),
            date('Y_m_d_His'),
            Str::snake($name)
        );

        return app()->basePath() .'/'.str_replace('\\', '/', $name).'.php';
    }


    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/../stubs/migration-tasks.stub';
    }

    /**
     * @param string $name
     */
    protected function makeDirectory(string $name): void
    {
        if (!$this->filesystem->isDirectory(dirname($this->getPath($name)))) {
            $this->filesystem->makeDirectory(dirname($this->getPath($name)), 0777, true, true);
        }
    }
}
