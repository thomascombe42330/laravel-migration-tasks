<?php

namespace Thomascombe\LaravelMigrationTasks\Deploy\Tasks;

abstract class MigrationTask
{
    /** @var string|null */
    public $flag = null;

    abstract public function up();
    abstract public function down();

    protected function getFlag()
    {
        return $this->flag;
    }
}
