<?php

namespace Thomascombe\LaravelMigrationTasks;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class LaravelMigrationTasks
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function getMigrationTasksToRun($paths)
    {
        /** @var Collection $runMigrations */
        $runMigrations = $this->getRunMigrations();
        return collect($this->getMigrationTasks($paths))->reject(function($value, $key) use ($runMigrations) {
            return $runMigrations->contains($key);
        });
    }

    protected function getMigrationTasks($paths)
    {
        return Collection::make($paths)->flatMap(function ($path) {
            return Str::endsWith($path, '.php') ? [$path] : $this->filesystem->glob($path.'/*_*.php');
        })->filter()->values()->keyBy(function ($file) {
            return $this->getMigrationName($file);
        })->sortBy(function ($file, $key) {
            return $key;
        })->all();
    }

    protected function getMigrationName(string $path)
    {
        return str_replace('.php', '', basename($path));
    }

    protected function getRunMigrations()
    {
        return DB::table('migration_tasks')->orderBy('batch')->orderBy('id')->pluck('migration_task');
    }
}
