<?php

namespace Thomascombe\LaravelMigrationTasks;

use Illuminate\Support\Facades\Facade;

class LaravelMigrationTasksFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel-migration-tasks';
    }
}
