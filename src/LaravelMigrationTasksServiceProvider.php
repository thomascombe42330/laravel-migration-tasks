<?php

namespace Thomascombe\LaravelMigrationTasks;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Thomascombe\LaravelMigrationTasks\Console\Commands\MakeMigrationTask;
use Thomascombe\LaravelMigrationTasks\Console\Commands\MigrationTask;

class LaravelMigrationTasksServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('laravel-migration-tasks.php'),
            ], 'config');

            // Registering package commands.
             $this->commands([
                 MakeMigrationTask::class,
                 MigrationTask::class,
             ]);

            $this->loadMigrationsFrom(__DIR__ . '/../resources/migrations');
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'laravel-migration-tasks');

        // Register the main class to use with the facade
        $this->app->singleton('laravel-migration-tasks', function () {
            return new LaravelMigrationTasks(app()->make(Filesystem::class));
        });
    }
}
