<?php

namespace Thomascombe\LaravelMigrationTasks\Tests;

use Orchestra\Testbench\TestCase;
use Thomascombe\LaravelMigrationTasks\LaravelMigrationTasksServiceProvider;

class ExampleTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [LaravelMigrationTasksServiceProvider::class];
    }
    
    /** @test */
    public function true_is_true()
    {
        $this->assertTrue(true);
    }
}
